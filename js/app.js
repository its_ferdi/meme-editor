define([
    // Defaults
    "jquery",
    "angular",
    "angularSanitize"
], function ($, ng) {
    "use strict";

    var popUp = {
        // Default Popup
        // ------------------------------------------------------------------------
        
        default: function(opts) {
            this.error(opts);
        },
        

        // Error Popup
        // ------------------------------------------------------------------------
        
        error: function(opts) {
            var $element = $('<div class="popup-wrap popup-error"> <div class="overlay"></div> <div class="popup"> <div class="pop-head"> <div class="pop-title"></div> <div class="close"><a class="pop-close"><i class="keepo-icon icon-cancel"></i></a></div> </div> <div class="pop-content"> <div class="text-area"> <div class="info"></div> </div> </div> </div> </div>'),
                $titleEl = $element.find('.pop-title'),
                $infoEl  = $element.find('.info'),
                $closeEl = $element.find('.pop-close'),
                options  = {
                    title   : 'Error !',
                    info    : 'Something went wrong :(',
                    onOpen  : void 0,
                    onClose : void 0
                };

            if (opts) { options = $.extend(options, opts); }

            // Already has error popup? dont show it again...it's ugly :(
            if ($('body').find('.popup-error').length) { return false; }

            // ------------------------------------------------------------------------
            
            $titleEl.html(options.title);
            $infoEl.html(options.info);

            $('body').append($element);

            if (typeof options.onOpen == 'function') {
                options.onOpen($element);
            }

            $element.on('click', 'a.pop-close', function(e) {
                e.preventDefault();

                if (typeof options.onClose == 'function') {
                    var ret = options.onClose($element);

                    if (typeof ret == 'boolean') { return ret; }
                }

                $element.remove();
            });
        },
    };

    var tokenService = {
        base: {},

        // ------------------------------------------------------------------------
        
        __today: function() { return Math.round((new Date()).getTime() / 1000); },

        __days: function(seconds) { return Math.round(seconds/86400); },

        __getToken: function($q, $http, callback) {
            var self     = this,
                provider = $http || $;

            return provider.get(this.base.baseURL + 'secret/token/get').then(
                function(response) {
                    var data                      = response.data || response;
                    window.localStorage.token     = JSON.stringify(data);
                    window.localStorage.tokenDate = self.__today();

                    if (callback && (typeof (callback) == 'function')) { callback(data); }

                    // Set AJAX globally
                    $.ajaxSetup({
                        beforeSend: function(xhr) {
                            xhr.setRequestHeader('Authorization', 'Bearer ' + data.token);
                        }
                    });

                    return {headers: {Authorization: 'Bearer ' + data.token}};
                },
                function(response) {
                    popUp.error({info: response.data.error_description || 'Something went wrong :('});
                    if ($q) { return $q.reject(response); }
                }
            );
        },

        __refreshToken: function($q, $http, callback) {
            var self     = this,
                provider = $http || $;

            return provider.get(this.base.baseURL + 'secret/token/refresh').then(
                function(response) {
                    var data                      = response.data || response;
                    window.localStorage.token     = JSON.stringify(data);
                    window.localStorage.tokenDate = self.__today();

                    if (callback && (typeof (callback) == 'function')) { callback(data); }

                    // Set AJAX globally
                    $.ajaxSetup({
                        beforeSend: function(xhr) {
                            xhr.setRequestHeader('Authorization', 'Bearer ' + data.token);
                        }
                    });

                    return {headers: {Authorization: 'Bearer ' + data.token}};
                },
                function(response) {
                    popUp.error({info: response.data.error_description || 'Something went wrong :('});
                    if ($q) { return $q.reject(response); }
                }
            );
        },

        getToken: function($q, $http, callback) { this.__getToken($q, $http, callback); },
        refreshToken: function($q, $http, callback) { this.__getToken($q, $http, callback); },

        accessToken: function($q, $http, callback) {
            var self        = this,
                token       = window.localStorage.token,
                tokenDate   = window.localStorage.tokenDate;

            if (token == undefined || tokenDate == undefined) {
                return this.__getToken($q, $http, callback);
            } else {
                var temp        = JSON.parse(token),
                    expiredDate = tokenDate - 0 + (10 * 24 * 60 * 60);

                // check JSON is not expired
                if ((expiredDate - this.__today()) < 1000) {
                    // token expired in less than a day, just request new token
                    return this.__refreshToken($q, $http, callback);
                } else {
                    // Set AJAX globally
                    $.ajaxSetup({
                        beforeSend: function(xhr) {
                            xhr.setRequestHeader('Authorization', 'Bearer ' + temp.token);
                        }
                    });

                    if ($q) {
                        // fixing return value so it can be received when the value is not from a promise >.<
                        return $q.resolve({headers: {Authorization: 'Bearer ' + temp.token}});
                    }
                }
            }
        },

        init: function(config) {
            if (config) { this.base = config; }
        }
    };

    var MainApp = {
        baseURL: 'http://alpha.keepo.me/',
        keepoApp: void 0,
        tokenService: void 0,


        // Default Popup
        // ------------------------------------------------------------------------
        
        defaultPopUp: function(opts) {
            popUp.default(opts);
        },
        

        // Error Popup
        // ------------------------------------------------------------------------
        
        errorPopUp: function(opts) {
            popUp.error(opts);
        },

        // Apps
        // ------------------------------------------------------------------------

        init: function() {
            var self = this;

            this.tokenService = tokenService;
            this.tokenService.init({baseURL : this.baseURL});
            this.tokenService.accessToken();

            // Set up Keepo App
            // ------------------------------------------------------------------------

            this.keepoApp = angular.module('keepoApp', ['ngSanitize']);

            this.keepoApp.config([
                '$compileProvider', '$interpolateProvider',
                function ($compileProvider, $interpolateProvider) {
                    // whitelist href content
                    $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|javascript):/);
                    // disable debug info
                    $compileProvider.debugInfoEnabled(false);
                    // change binding notation
                    $interpolateProvider.startSymbol('<@');
                    $interpolateProvider.endSymbol('@>');
                }
            ]);

            this.keepoApp.factory("accessToken", function ($http, $q, $window) {
                return {
                    get: function () {
                        return self.tokenService.accessToken($q, $http);
                    },
                    refresh: function () {
                        return self.tokenService.refreshToken($q, $http);
                    }
                }
            });

            // ------------------------------------------------------------------------

            this.keepoApp.factory('signIn', function ($http, $q, accessToken) {
                return {
                    get: function (data) {
                        var api_url = MainApp.baseURL + 'api/v1/auth/login';
                        // get the token to access api
                        return accessToken.get().then(
                            function (token) {
                                // inject parameter query string
                                // token received, now get the feeds data
                                return $http.post(api_url, data, token).then(
                                    function (response) {
                                        // if status 401, remove localstorage if exists
                                        if (response.status == '401' && window.localStorage.token) {
                                            window.localStorage.removeItem('token');
                                            window.localStorage.removeItem('tokenDate');

                                            response.data.error_description = 'Something went wrong, please try again';
                                        }

                                        return response.data;
                                    },
                                    function (response) {
                                        return $q.reject(response.data);
                                    }
                                );
                            }
                        )
                    }
                }
            });

            this.keepoApp.controller('LoginController', ['$scope', 'signIn', function ($scope, signIn) {
                var normalLogin = 'Login';
                var processLogin = 'logging in..please wait';
                $scope.login ={};

                if (window.loginError) {
                    $scope.$parent.popuplogin = 1;
                    $scope.$parent.popuperror = 1;
                    $scope.$parent.errormsg   = window.loginError;
                }

                /**
                 * function for change button
                 * @private
                 */
                var __changeButton = function () {
                    $scope.login.buttonLogin = normalLogin;
                    if ($scope.$parent.loggingin) {
                        $scope.login.buttonLogin = processLogin;
                    }
                };

                 // make sure button has the text
                __changeButton();

                $scope.signin = function () {
                    // flag when login in process
                    $scope.$parent.loggingin = 1;
                    // prepare data to be posted
                    /*var userData = {
                        username: $scope.login.user,
                        password: $scope.login.password
                    };
                    // show loading button
                    __changeButton();

                    /*signIn.get(userData).then(function (data) {
                        // console.log(data);
                        // clean password on scope
                        // $scope.login.password = undefined;
                        // $scope.$parent.popuplogin=0;
                        // $scope.$parent.loggingin = 0;

                        // Reload the page
                        window.location.reload();

                        __changeButton();
                    }, function (data) { // on error
                        //$scope.$parent.popuplogin=0;
                        $scope.$parent.errormsg = data.error_description;
                        $scope.$parent.popuperror=1;
                        $scope.$parent.loggingin = 0;
                        __changeButton();
                    });*/

                    // ------------------------------------------------------------------------
                    
                    $('.pop-login').find('form').submit();
                }
            }]);
            // ------------------------------------------------------------------------

            // Subscribe Controller
            // ------------------------------------------------------------------------
            
            if ($('[ng-controller=subscribeController]')) {
                MainApp.keepoApp.controller('subscribeController', ['$scope', '$element', '$http', function($scope, $element, $http) {
                    $scope.email      = void 0;
                    $scope.processing = false;

                    $scope.submit = function($event) {
                        if (! $scope.email) { return false; }
                        if ($scope.processing) { return false; }

                        $scope.processing = true;

                        // TODO : do request
                        // ------------------------------------------------------------------------
                        
                        console.log('subscribing');
                    };
                }]);
            }
            
            // ------------------------------------------------------------------------
            // ------------------------------------------------------------------------


            // Load App
            var appName = $('.main-container').attr('data-app');
            if (appName) {

                require(['apps/' + appName + '/' + appName + '.module'], function(App) {
                    App.init(self);
                });
            }
        }
    };

    // set to global variable
    window.MainApp = MainApp;

    return MainApp;
});